﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollUVs : MonoBehaviour {

    public int materialIndex = 0;
    public Vector2 uvAnimationRate = new Vector2(1.0f, 0.0f);
    public string textureName = "_MainTex";

    Vector2 uvOffset = Vector2.zero;

    public float m_xSpeedMultiplier = 1.0f;
    public float m_ySpeedMultiplier = 1.0f;

    public Renderer m_renderer;

    private void Awake()
    {
        if (m_renderer == null)
            m_renderer = GetComponent<Renderer>();
    }

    void LateUpdate()
    {
        Vector2 scrollRate = uvAnimationRate;
        scrollRate.x *= m_xSpeedMultiplier;
        scrollRate.y *= m_ySpeedMultiplier;

        uvOffset += (scrollRate * Time.deltaTime);

        if (m_renderer.enabled) {
            m_renderer.materials[materialIndex].SetTextureOffset(textureName, uvOffset);
        }
    }
}
