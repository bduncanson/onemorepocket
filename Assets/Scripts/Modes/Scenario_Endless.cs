﻿/*
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scenario_Endless : Scenario {


    public override void Update()
    {
        base.Update();
    }

    public override void SpawnSection()
    {
        if (m_disableSpawningSegments) 
            return;

        base.SpawnSection();

        int id = _activeWalls.Count;

        GameObject g = Instantiate(m_wallPrefabs[Random.Range(0, m_wallPrefabs.Length)]);

        //g.transform.position = new Vector3(0f, 0f, id * 30f);

        if (_activeWalls.Count > 0)
            g.transform.position = new Vector3(0f, 0f, _activeWalls[_activeWalls.Count - 1].ExitZPos());

        g.transform.parent = m_wallHolder;
        _activeWalls.Add(g.GetComponent<TableSegment>());
    }

    public override void SetupRun()
    {
        base.SetupRun();

        if (TopDistance() > 0.0f)
            m_topDistanceMarker.position = new Vector3(0f, 0f, TopDistance());
        else
            m_topDistanceMarker.position = new Vector3(0f, 0f, -50f);

        m_uiController.ShowHideFailPanel(false);

        m_player.OnReset();

        SetAcceptInput(true);
        SetRunReadyToBeSetup(false);

        for (int i = 0; i < _activeBalls.Count; i++) {
            PoolManager.Inst.ReturnBall(_activeBalls[i]);
            //Destroy(_activeBalls[i]);
        }

        for (int i = 0; i < _activeWalls.Count; i++) {
            Destroy(_activeWalls[i].gameObject);
        }

        SetCurrentScore(0);
        SetCurrentDistance(0f);

        _activeWalls.Clear();
        _activeBalls.Clear();

        if (!m_disableSpawningSegments)
        {
            // Spawn the walls pieces
            for (int i = 0; i < 5; i++)
            {
                GameObject g = null;

                // spawn an intro piece
                if (i == 0)
                {
                    g = Instantiate(m_wallPrefabs_Intros[Random.Range(0, m_wallPrefabs_Intros.Length)]);
                    g.transform.position = new Vector3(0f, 0f, 0f);
                }
                else
                {
                    g = Instantiate(m_wallPrefabs[Random.Range(0, m_wallPrefabs.Length)]);
                }

                g.transform.parent = m_wallHolder;
                _activeWalls.Add(g.GetComponent<TableSegment>());

                if (i > 0)
                    g.transform.position = new Vector3(0f, 0f, _activeWalls[_activeWalls.Count - 2].ExitZPos());


                // for each segment, spawn balls near each pair of pockets
                for (int p = 0; p < 2; p++)
                {
                    // 5 - 12, pair 1
                    // 17 - 24, pair 2

                    int balls = Random.Range(2, 4);                 
                }
            }
        }
        else
        {
            // Align all manually placed segments properly
            TableSegment[] ts = m_wallHolder.GetComponentsInChildren<TableSegment>();

            for (int i = 0; i < ts.Length; i++)
            {
                if (i == 0) {
                    ts[i].transform.position = new Vector3(0f, 0f, 0f);
                }
                else {
                    ts[i].transform.position = new Vector3(0f, 0f, ts[i - 1].ExitZPos());
                }
            }
        }

        m_camera.transform.position = new Vector3(m_camera.transform.position.x, m_camera.transform.position.y, m_player.transform.position.z + cameraFollowOffset);

        ResetLives();
    }

    public override void ExitedSection()
    {
        base.ExitedSection();

        // todo: change this to use pooling and if we can't go back far, clear out old sections

        // create a new wall
        SpawnSection();
    }

    public override void BallPocketed(OMP_Ball b, OMP_Ball.BallType type, int ball)
    {
        base.BallPocketed(b, type, ball);

        if (type == OMP_Ball.BallType.Eight || ball == 8)
            EndRun("Pocketed 8-ball.");
        else if (type == OMP_Ball.BallType.Standard)
        {
            AddScore(1);
            ResetLives();
            m_player.BallPocketedThisTurn(true);
        }
        else if (type == OMP_Ball.BallType.Powerup)
        {
            ResetLives();
            m_player.BallPocketedThisTurn(true);

            // check powerup type
            if (ball == 0)
            {
                // collision trajectory marker
                m_player.CollisionTrajectoryPowerup(5.0f);
            }
            else if (ball == 1)
            {
                // longer player trajectory marker
            }
        }

        PoolManager.Inst.ReturnBall(b);

        // could do checks later to make sure we are pocketing the right ball, etc
        // bigs vs smalls
        // ordered?
    }
}
*/