﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using MoreMountains.NiceVibrations;

using GameAnalyticsSDK;

public class Scenario : MonoBehaviour {

    public static Scenario Inst;

    public enum GameState { Menu, SettingUp, WaitingToStart, Active, Completed, Failed }
    private GameState gameState = GameState.SettingUp;

    public enum EndReason { Eightball, SelfPocket, NoLives, LevelComplete }

    [Header("References")]
    public OMP_Player m_player;
    public Camera m_camera;
    public UIOMPocket m_uiController;

    public GameObject[] m_ballPrefabs;
    public GameObject[] m_wallPrefabs;
    public GameObject[] m_wallPrefabs_Intros;       // intro pieces to use for normal levels
                                                    // do we need outro pieces too?
    public GameObject[] m_wallPrefabs_Tutorials;    // Level 1-X (5?) use premade tutorial pieces
    public GameObject m_wallEndCap;

    public ParticleSystem m_goalParticlesLeft;      // fire confetti when player reaches goal 
    public ParticleSystem m_goalParticlesRight;     // fire confetti when player reaches goal

    public Transform m_wallHolder;
    public Transform m_ballHolder;
    public Transform m_topDistanceMarker;

    [Header("Camera Settings")]
    public bool m_clampZPos = false;
    public float m_zClampMin = 0.0f;
    public float m_zClampMax = 0.0f;
    public float m_followSpeed = 1.5f;
    public float m_previewSpeed = 2.5f;
    public float m_previewEndSpeed = 6.5f;
    private Vector3 lastCameraPosition;

    [Header("Settings")]
    public float cameraFollowOffset = 10.0f;
    public bool m_endRunOnMiss = false; // should we end the run if the player doesnt hit any balls on a shot
    public bool m_disableSpawningSegments = false;  // force scenario to stop spawning new segments

    [Header("Magnetic")]
    [Range(0f, 1f)]
    public float m_magneticPocketRange = 0.2f;

    // Active Objects
    public List<OMP_Ball> _activeBalls = new List<OMP_Ball>();
    public List<TableSegment> _activeWalls = new List<TableSegment>();

    // Scoring
    private int _currentScore = 0;
    private int _topScore = 0;
    private float _currentDistance = 0.0f;
    private float _topDistance = 0.0f;

    // Misc
    private bool _acceptInput = true;   // is input currently being recieved
    private bool _runActive = false;
    private bool _runReadyToSetup = false;

    private float cameraPanFingerDownYPos = 0f;
    private bool cameraPanPreviewActive = false;

    [Header("Debug")]
    public bool m_forceMagneticPocketsActive = false;

    public bool m_forceLevel = false;
    public int m_levelToForceLoad = -1;

    private List<BallState> ballStates = new List<BallState>(); // track ball states at start of turn

    private void Awake() {
        Inst = this;
     
        #if UNITY_STANDALONE
        Screen.SetResolution(640, 1136, false);
        Screen.fullScreen = true;
        #endif       

        #if UNITY_IOS
		MMVibrationManager.iOSInitializeHaptics ();
        #endif

        GameAnalytics.Initialize();
    }

    private void OnDisable() {
        #if UNITY_IOS
        MMVibrationManager.iOSReleaseHaptics();
        #endif
    }

    public virtual void Start()
    {
        // load in the players' highest level obtained
        lastCompletedLevel = PlayerPrefs.GetInt("level.completed", 0);
        currentLevel = lastCompletedLevel + 1;

        InitialiseSeed();

        #if UNITY_STANDALONE
        Screen.SetResolution(640, 1136, false);
        Screen.fullScreen = true;
        #endif

        float desiredAspect = 9f / 16f;
        float aspect = Camera.main.aspect;
        float ratio = desiredAspect / aspect;
        m_camera.orthographicSize = m_camera.orthographicSize * ratio;

        SetupRun();
    }

    public virtual void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
            EndRun(EndReason.NoLives);

        #if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Escape))
            SceneManager.LoadScene(0);
        #endif

        if (Input.GetMouseButtonDown(0) && _runReadyToSetup)
        {
            if (!_runActive)
            {
                if (lastCompletedLevel == currentLevel)
                    LoadNextLevel();
                else
                    SetupRun();
            }
        }

        lastCameraPosition = m_camera.transform.position;
    }

    public virtual void LateUpdate()
    {
        if (_runActive) {
            _currentDistance = m_player.transform.position.z; // - m_player.InitPlayerPosition().z;
        }

        CameraMovement();
    }


    private void SetGameState(GameState newState) {

        switch (newState)
        {
            case GameState.Menu:
                break;
            case GameState.SettingUp:
                break;
            case GameState.WaitingToStart:
                // activate input
                break;
            case GameState.Active:
                GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "game");
                break;
            case GameState.Completed:

                m_goalParticlesLeft.Emit(50);
                m_goalParticlesRight.Emit(50);

                // Partycles
                MMVibrationManager.Haptic(HapticTypes.Success);
                EmitCelebrationParticles();

                // set this level as completed
                lastCompletedLevel = currentLevel;
                PlayerPrefs.SetInt("level.completed", currentLevel);

                // Analytics event
                GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "game", CurrentScore());

                // Delay and then load next level
                LoadNextLevel(1.5f);
                break;
            case GameState.Failed:
                GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "game", CurrentScore());
                StartCoroutine(DelayInputEnable());
                break;
            default:
                break;
        }

        gameState = newState;
    }    

    public void OnPocketedPlayer()
    {
        // Pocketed yourself;
        SetGameState(GameState.Failed);
    }

    public void OnPocketedEightball()
    {
        // Pocketed eight ball;
        SetGameState(GameState.Failed);
    }

    public void EndRun(EndReason reason)
    {
        if (!_runActive)
            return;

        // set run over for player
        m_player.OnRunEnded();

        _acceptInput = false;
        _runActive = false;
        _runReadyToSetup = false;

        if (_currentScore > _topScore)
            _topScore = _currentScore;

        if (_currentDistance > _topDistance)
            _topDistance = _currentDistance;

        //currentState = ls;

        // UI stuff
        switch (reason)
        {
            case EndReason.Eightball:
                m_uiController.RunFailed("Pocketed eight-ball");
                SetGameState(GameState.Failed);
                break;
            case EndReason.SelfPocket:
                m_uiController.RunFailed("Pocketed yourself");
                SetGameState(GameState.Failed);
                break;
            case EndReason.NoLives:
                m_uiController.RunFailed("Ran out of lives");
                SetGameState(GameState.Failed);
                break;
            case EndReason.LevelComplete:
                m_uiController.LevelCompleted("Completed level: " + currentLevel);
                SetGameState(GameState.Completed);
                break;
            default:
                break;
        }
    }

    public GameState CurrentGameState() {
        return gameState;
    }

    public virtual void CameraMovement()
    {
        if (!m_player.ShotActive())
        {
            #if UNITY_STANDALONE || UNITY_EDITOR
            if (Input.GetMouseButtonDown(1)) {
                cameraPanFingerDownYPos = Input.mousePosition.y;
                cameraPanPreviewActive = true;
            }

            if (Input.GetMouseButton(1)) {
                float yMovement = 0f;
                yMovement = Input.mousePosition.y - cameraPanFingerDownYPos;
                yMovement = Mathf.Clamp(yMovement, -40f, 40f);
                m_camera.transform.position = Vector3.Lerp(m_camera.transform.position, new Vector3(m_camera.transform.position.x, m_camera.transform.position.y, CameraZPosition(m_player.transform.position.z + cameraFollowOffset - yMovement)), Time.deltaTime * m_previewSpeed);
            }
            else
            {
                if (cameraPanPreviewActive)
                    m_camera.transform.position = Vector3.Lerp(m_camera.transform.position, new Vector3(m_camera.transform.position.x, m_camera.transform.position.y, CameraZPosition(m_player.transform.position.z + cameraFollowOffset)), Time.deltaTime * m_previewEndSpeed);
                else if (m_player.CurrentState() != OMP_Player.State.Placing)
                    m_camera.transform.position = Vector3.Lerp(m_camera.transform.position, new Vector3(m_camera.transform.position.x, m_camera.transform.position.y, CameraZPosition(m_player.transform.position.z + cameraFollowOffset)), Time.deltaTime * m_followSpeed);


                if (cameraPanPreviewActive)
                {
                    //if (Mathf.Abs((m_player.transform.position.z + cameraFollowOffset) - m_camera.transform.position.z) < 0.5f)
                    //  cameraPanPreviewActive = false;

                    if (Vector3.Distance(m_camera.transform.position, lastCameraPosition) < 0.05f)
                        cameraPanPreviewActive = false;

                    //cameraPanPreviewActive = false;
                }
            }
            #else
           
            if (Input.touchCount == 2) {
                float yMovement = 0f;
                cameraPanPreviewActive = true;

                if (Input.GetTouch(1).phase == TouchPhase.Began) {
                    cameraPanFingerDownYPos = Input.GetTouch(0).position.y;
                }
                else {
                    yMovement = Input.GetTouch(0).position.y - cameraPanFingerDownYPos;
                    yMovement = Mathf.Clamp(yMovement, -20f, 20f); 
                    m_camera.transform.position = Vector3.Lerp(m_camera.transform.position, new Vector3(m_camera.transform.position.x, m_camera.transform.position.y, CameraZPosition(m_player.transform.position.z + cameraFollowOffset - yMovement)), Time.deltaTime * m_previewSpeed);
                }
            }
            else
            {
                if (Mathf.Abs((m_player.transform.position.z + cameraFollowOffset) - m_camera.transform.position.z) < 0.5f)
                    cameraPanPreviewActive = false;

                if (cameraPanPreviewActive)
                    m_camera.transform.position = Vector3.Lerp(m_camera.transform.position, new Vector3(    
                        m_camera.transform.position.x, 
                        m_camera.transform.position.y, 
                        CameraZPosition(m_player.transform.position.z + cameraFollowOffset)), Time.deltaTime * m_previewSpeed);
                else if (m_player.CurrentState() != OMP_Player.State.Placing)
                    m_camera.transform.position = Vector3.Lerp(m_camera.transform.position, new Vector3(m_camera.transform.position.x, m_camera.transform.position.y, CameraZPosition(m_player.transform.position.z + cameraFollowOffset)), Time.deltaTime * m_followSpeed);
            }          
            #endif
        }
        else {
            if (m_player.CurrentState() != OMP_Player.State.Placing)
                m_camera.transform.position = Vector3.Lerp(m_camera.transform.position, new Vector3(m_camera.transform.position.x, m_camera.transform.position.y, CameraZPosition(m_player.transform.position.z + cameraFollowOffset)), Time.deltaTime * m_followSpeed);
        }
    }

    public float CameraZPosition(float z)
    {
        if (m_clampZPos)
            return Mathf.Clamp(z, m_zClampMin, m_zClampMax);
        else
            return z;
    }

    IEnumerator DelayInputEnable() {
        yield return new WaitForSeconds(0.25f);
        _runReadyToSetup = true;
    }

    // Spawn a new section
    public virtual void SpawnSection() {
    }

    public virtual void SetupRun() {
        //startingBallsInCurrentLevel = 0;

        if (CurrentGameState() == GameState.Failed) {
            SetCurrentScore(0);
        }

        SetGameState(GameState.SettingUp);

        InitialiseSeed();
    }

    public virtual void StartRun() {
        //startingBallsInCurrentLevel = PoolManager.Inst.ActiveBallCount();
        _acceptInput = true;
        _runActive = true;
        SetGameState(GameState.Active);
    }

    public virtual void StartTurn() {
        // track position of all active balls at the start of a turn

        ballStates.Clear();

        for (int i = 0; i < _activeBalls.Count; i++) {

            BallState bS = new BallState();

            bS.m_ball = _activeBalls[i];
            bS.m_activeAtTurnStart = true;
            bS.m_positionAtTurnStart = _activeBalls[i].transform.position;
            ballStates.Add(bS);

            _activeBalls[i].OnTurnStart();
        }
    }

    // Rewind last turn and revert all ball pockets/score/etc
    public virtual void RestartTurn()
    {
        for (int i = 0; i < _activeBalls.Count; i++) {
            _activeBalls[i].OnTurnReset();
        }

        //for (int i = 0; i < ballStates.Count; i++) {
        //    if (!ballStates[i].m_ball.gameObject.activeSelf) {
        //        ballStates[i].Respawn();
        //    }
        //}

        OMP_Player.Inst.OnTurnReset();
    }

    public bool RunActive() {
        return _runActive;
    }

    // When a ball is pocketed
    public virtual void BallPocketed(OMP_Ball b, OMP_Ball.BallType type, int ball) {
        if (ball == 8)
            CreateCelebration(b.transform.position, true);
        else
            CreateCelebration(b.transform.position);

        if (ball != 8) {
            ballsPocketedThisAttempt += 1;
        }

        MMVibrationManager.Haptic(HapticTypes.HeavyImpact);
    }

    public virtual void DestroyBall(GameObject g)
    {
        PoolManager.Inst.ReturnBall(g.GetComponent<OMP_Ball>());
    }

    // What do we do if the player exits a section
    public virtual void ExitedSection() {
       
    }

    public void OnLevelSetup() {
        // reset balls pocketed count
        ballsPocketedThisAttempt = 0;
        SetGameState(GameState.WaitingToStart);
    }

    /// -------------------------
    /// SHOT
    /// -------------------------
    public virtual void OnShotEnded(bool hit, bool pocketed) {

        if (hit == false)
        {
            // lose a life
            if (m_endRunOnMiss && !hit) {
                //EndRun("Didn't hit a ball.");
            }
            else
                AddLives(-1);
        }
        else
        {
            // we good son
            if (!pocketed)
            {
                AddLives(-1);
                ResetMultiplier();
            }
        }
    }

    public virtual void OnBallHit(int hit) {

    }

    /// -------------------------
    /// INPUT
    /// -------------------------
    public bool AcceptInputDrag()
    {
        if (m_player.CanDrag() && _acceptInput && !cameraPanPreviewActive)
            return true;

        return false;
    }

    public bool AcceptInput()
    {
        if (cameraPanPreviewActive)
            return false;
        return _acceptInput;
    }

    public void SetAcceptInput(bool accept) {
        _acceptInput = accept;
    }

    public void SetRunReadyToBeSetup(bool ready) {
        _runReadyToSetup = ready;
    }



    /// -------------------------
    /// SCORING
    /// -------------------------

    public int CurrentScore() { return _currentScore; }
    public void SetCurrentScore(int score) { _currentScore = score; }
    public void AddScore(int score) { _currentScore += score; }

    public int TopScore() { return _topScore; }
    public void SetTopScore(int score) { _topScore = score; }

    public float CurrentDistance() { return _currentDistance; }
    public void SetCurrentDistance(float distance) { _currentDistance = distance; }

    public float TopDistance() { return _topDistance; }
    public void SetTopDistance(float distance) { _topDistance = distance; }

    // Sink Multiplier
    private int _multiplier = 0;

    public int CurrentMultiplier() { return _multiplier; }

    public void AddMultiplier()
    {
        _multiplier++;
    }

    public void ResetMultiplier()
    {
        _multiplier = 0;
    }

    // Lives 
    private int _currentLives = 3;

    public int LivesRemaining()
    {
        return _currentLives;
    }

    public void AddLives(int lives)
    {
        _currentLives = Mathf.Clamp(_currentLives + lives, 0, 3);

        if (_currentLives == 0)
        {
            EndRun(EndReason.NoLives);
        }
    }

    public void ResetLives()
    {
        _currentLives = 3;
    }

    public void AddActiveBall(OMP_Ball b) {
        _activeBalls.Add(b);
    }

    /// <summary>
    /// POWERUP
    /// </summary>

    [Header("Powerups")]
    public float m_magneticPocketForceMulti = 2.0f;

    public bool MagneticPocketsActive()
    {
        if (m_forceMagneticPocketsActive)
            return true;
        else
        {
            // if powerup active
        }

        return false;
    }

    public float MagneticPocketForceMultiplier() {
        return m_magneticPocketForceMulti;
    }

    public float MagneticPocketRange() {
        return m_magneticPocketRange;
    }


    [Header("Pocket Celebration")]
    public GameObject m_pocketParticles;
    public GameObject m_pocketParticlesFail;

    // POCKET CELBRATION
    public void CreateCelebration(Vector3 pocketPos, bool fail = false)
    {
        if (m_pocketParticles == null)
            return;

        GameObject g = null;
        if (!fail)
            g = Instantiate(m_pocketParticles);
        else
        {
            if (m_pocketParticlesFail == null)
                g = Instantiate(m_pocketParticles);
            else
                g = Instantiate(m_pocketParticlesFail);
        }

        g.transform.position = pocketPos;

        // rotate towards center of table?
        if (g.transform.position.x > 0f)
            g.transform.eulerAngles = new Vector3(0f, 270f, 0f);

        Destroy(g, 2.0f);
    }



    /// -------------------------
    /// LEVELS
    /// -------------------------
    private int lastCompletedLevel = 0;     // what was the last ocmpleted level
    private int currentLevel = 1;           // track player progression

    public int CurrentLevel()
    {
        // force a specific level to be loaded
        #if UNITY_EDITOR
        if (m_forceLevel && m_levelToForceLoad > 0)
            return m_levelToForceLoad;
        #endif

        return currentLevel;
    }

    private void LoadNextLevel(float delay = 0f) {

        if (delay > 0f)
            StartCoroutine(LoadLevelDelay(delay));
        else
            LoadLevel(currentLevel + 1);
    }

    IEnumerator LoadLevelDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        LoadLevel(currentLevel + 1);
    }

    public void LoadLevel(int level)
    {
        ThemeManager.Inst.NextTheme(level);
        Debug.Log("loading level: " + level);
        currentLevel = level;
        SetupRun();
    }

    private int ballsRequiredToCompleteLevel = 2;      // how many balls the player needs to pocket in order to complete the level
    private int ballsPocketedThisAttempt = 0;           // how many balls have been pocketed this run

    public int BallsPocketedThisAttempt() {
        return ballsPocketedThisAttempt;
    }

    public int TotalBallsRequiredToCompleteLevel() {
        return ballsRequiredToCompleteLevel;
    }

    public void SetTotalBallsRequired(int req) {
        ballsRequiredToCompleteLevel = req;
    }

    // How many balls have been pocketed
    public float CurrentLevelProgress() {
        //if (startingBallsInCurrentLevel > 0)
        //    return (startingBallsInCurrentLevel - PoolManager.Inst.ActiveBallCount()) / (float)startingBallsInCurrentLevel;

        return BallsPocketedThisAttempt() / (float)TotalBallsRequiredToCompleteLevel();
    }

    public string CurrentLevelProgressString() {
        return BallsPocketedThisAttempt() + "/" + TotalBallsRequiredToCompleteLevel();
    }

    [ContextMenu("Reset Level Progress")]
    public void ResetLevelProgress() {
        PlayerPrefs.SetInt("level.completed", 0);
        currentLevel = 1;

        SetupRun();
    }


    // -----------------------------------------------
    // SEED
    // -----------------------------------------------

    [Header("Seed")]
    public SeedType m_seedType = SeedType.Level;
    public enum RandomType { Default, SegmentType, Ballspawn }
    public enum SeedType { Level, Overide, Daily, Random }
    public int m_overrideSeedVal = 1337;
    private System.Random spawnerRandom;    // Random used for spawning segments
    private System.Random ballSpawnRandom;
    private System.Random defaultRandom;

    private int currentSeed = -1;

    private void InitialiseSeed()
    {
        if (m_seedType == SeedType.Level) {
            SetSeed(RandomType.SegmentType, CurrentLevel());
            SetSeed(RandomType.Ballspawn, CurrentLevel());
        }
        if (m_seedType == SeedType.Overide) {
            SetSeed(RandomType.SegmentType, m_overrideSeedVal);
            SetSeed(RandomType.Ballspawn, m_overrideSeedVal);
        }
        else if (m_seedType == SeedType.Daily) {
            SetSeed(RandomType.SegmentType, DailySeed());
            SetSeed(RandomType.Ballspawn, DailySeed());
        }
        else if (m_seedType == SeedType.Random) {
            SetSeed(RandomType.SegmentType, RandomSeed());
            SetSeed(RandomType.Ballspawn, RandomSeed());
        }

        SetSeed(RandomType.Default, CurrentLevel());
    }

    // Set the current run
    private void SetSeed(RandomType rType, int seed) {

        if (rType == RandomType.SegmentType)
            spawnerRandom = new System.Random(seed);
        else if (rType == RandomType.Ballspawn)
            ballSpawnRandom = new System.Random(seed);
        else
            defaultRandom = new System.Random(seed);

        currentSeed = seed;
    }

    // Return today's date seed
    private int DailySeed() {
        return int.Parse(CurrentTimePST().Date.Year.ToString() + CurrentTimePST().Date.Month.ToString("D2") + CurrentTimePST().Date.Day.ToString("D2"));
    }

    // Return a random seed
    private int RandomSeed() {
        string seed = System.DateTime.Now.Ticks.ToString("D5");
        seed = seed.Substring(seed.Length - 5, 5);
        return int.Parse(seed);
    }

    // Current seed being used
    public int CurrentSeed() {
        return currentSeed;
    }


    // ---------------------------------
    // RANDOM
    // ---------------------------------

    public int NextRandom(RandomType rType) {
        if (rType == RandomType.SegmentType)
            return spawnerRandom.Next();
        else if (rType == RandomType.Ballspawn)
            return ballSpawnRandom.Next();
        else return defaultRandom.Next();
    }

    public int NextRandom(RandomType rType, int maxValue) {
        if (rType == RandomType.SegmentType)
            return spawnerRandom.Next(maxValue);
        else if (rType == RandomType.Ballspawn)
            return ballSpawnRandom.Next(maxValue);
        else return defaultRandom.Next(maxValue);
    }

    public int NextRandom(RandomType rType, int minValue, int maxValue)
    {
        if (rType == RandomType.SegmentType)
            return spawnerRandom.Next(minValue, maxValue);
        else if (rType == RandomType.Ballspawn)
            return ballSpawnRandom.Next(minValue, maxValue);
        else return defaultRandom.Next(minValue, maxValue);
    }

    public System.Random RandomBallSpawn() {
        return ballSpawnRandom;
    }

    public double GetRandomBallSpawn(double minimum, double maximum) {
        return ballSpawnRandom.NextDouble() * (maximum - minimum) + minimum;
    }

    // ---------------------------------
    // TIME
    // ---------------------------------

    public DateTime CurrentTimePST() {
        // -7 for pdt, -8 for pst, who knows what gplay resets at =/
        return DateTime.UtcNow.AddHours(-8);
    }

    // Will use these for checking reset times for iOS leaderboards
    public DateTime DailyRolloverTime() {
        return CurrentTimePST().AddDays(1).Date;
    }




    // ---------------------------------
    // PARTY-CLES
    // ---------------------------------

    public ParticleSystem m_celebrationParticles;

    public void EmitCelebrationParticles()
    {
        m_celebrationParticles.Play();
    }

}

public class BallState
{
    public OMP_Ball m_ball;
    public bool m_activeAtTurnStart = true;
    public Vector3 m_positionAtTurnStart;

    public void Respawn()
    {
        m_ball.transform.position = m_positionAtTurnStart;
        m_activeAtTurnStart = true;
        m_ball.gameObject.SetActive(true);
    }
}