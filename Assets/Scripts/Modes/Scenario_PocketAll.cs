﻿/*
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scenario_PocketAll : Scenario {

    [Header("PocketAll Settings")]
    public float m_xRangeMin;
    public float m_xrangeMax;
    public float m_zRangeMin;
    public float m_zRangeMax;

    public override void Update()
    {
        base.Update();
    }

    public override void SetupRun()
    {
        base.SetupRun();

        m_uiController.ShowHideFailPanel(false);

        m_player.OnReset();

        SetAcceptInput(true);
        SetRunReadyToBeSetup(false);

        for (int i = 0; i < _activeBalls.Count; i++) {
            Destroy(_activeBalls[i]);
        }

        for (int i = 0; i < _activeWalls.Count; i++) {
            Destroy(_activeWalls[i]);
        }

        SetCurrentScore(0);
        SetCurrentDistance(0f);

        _activeWalls.Clear();
        _activeBalls.Clear();

        // Spawn One of each ball
        for (int i = 0; i < m_ballPrefabs.Length; i++)
        {
            GameObject ball = Instantiate(m_ballPrefabs[i]);
            ball.transform.position = new Vector3(Random.Range(m_xRangeMin, m_xrangeMax), 0.5f, Random.Range(m_zRangeMin, m_zRangeMax));
            _activeBalls.Add(ball.GetComponent<OMP_Ball>());
            ball.transform.parent = m_ballHolder;
        }
 
        // place camera
        m_camera.transform.position = new Vector3(m_camera.transform.position.x, m_camera.transform.position.y, m_player.transform.position.z + cameraFollowOffset);

        ResetLives();
    }

    public override void BallPocketed(OMP_Ball b, OMP_Ball.BallType type, int ball)
    {
        base.BallPocketed(b, type, ball);

        if (type == OMP_Ball.BallType.Eight || ball == 8)
        {
            if (CurrentScore() < 14)
                EndRun("Pocketed 8-ball.");
            else
                EndRun("Won!");
        }
        else if (type == OMP_Ball.BallType.Standard)
        {
            AddScore(1);
            ResetLives();
            m_player.BallPocketedThisTurn(true);
        }
        else if (type == OMP_Ball.BallType.Powerup)
        {
            ResetLives();
            m_player.BallPocketedThisTurn(true);

            // check powerup type
            if (ball == 0)
            {
                // collision trajectory marker
                m_player.CollisionTrajectoryPowerup(5.0f);
            }
            else if (ball == 1)
            {
                // longer player trajectory marker
            }
        }

        Destroy(b.gameObject);

        // could do checks later to make sure we are pocketing the right ball, etc
        // bigs vs smalls
        // ordered?
    }
}
*/