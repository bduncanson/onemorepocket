﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scenario_EndlessLevels : Scenario {

    public bool m_useEndCap = false;

    public AnimationCurve m_difficultyCurve;

    public override void Update()
    {
        base.Update();
    }

    public override void SpawnSection()
    {
        if (m_disableSpawningSegments) 
            return;

        base.SpawnSection();

        int id = _activeWalls.Count;

        var prefab = m_wallPrefabs[NextRandom(RandomType.SegmentType, 0, m_wallPrefabs.Length)];

        while (CurrentLevel() < prefab.GetComponent<TableSegment>().m_minLevelRequirement) {
            prefab = m_wallPrefabs[NextRandom(RandomType.SegmentType, 0, m_wallPrefabs.Length)];
        }

        GameObject g = Instantiate(prefab);

        if (_activeWalls.Count > 0)
            g.transform.position = new Vector3(0f, 0f, _activeWalls[_activeWalls.Count - 1].ExitZPos());

        g.transform.parent = m_wallHolder;

        TableSegment ts = g.GetComponent<TableSegment>();
        _activeWalls.Add(ts);
        ts.Initialize();
    }

    public override void SetupRun()
    {
        base.SetupRun();

        //if (TopDistance() > 0.0f)
        //    m_topDistanceMarker.position = new Vector3(0f, 0f, TopDistance());
        //else
        //    m_topDistanceMarker.position = new Vector3(0f, 0f, -50f);

        m_uiController.ShowHideFailPanel(false);

        m_player.OnReset();

        SetAcceptInput(true);
        SetRunReadyToBeSetup(false);

        // Return any balls that are currently active
        for (int i = 0; i < _activeBalls.Count; i++) {
            PoolManager.Inst.ReturnBall(_activeBalls[i]);
        }

        // Clean up any active walls: 
        // todo: pool these prefab pieces
        for (int i = 0; i < _activeWalls.Count; i++) {
            Destroy(_activeWalls[i].gameObject);
        }

        //SetCurrentScore(0);
        SetCurrentDistance(0f);

        _activeWalls.Clear();
        _activeBalls.Clear();

        int curLevel = CurrentLevel();

        // Setup difficulty
        int req = m_wallPrefabs_Tutorials.Length + 4; // base of 5

        if (curLevel < m_wallPrefabs_Tutorials.Length) {
            req = curLevel + 4; // start off as 5
        } 
        else {
            req += (int)m_difficultyCurve.Evaluate(CurrentLevel() - m_wallPrefabs_Tutorials.Length);
        }

        SetTotalBallsRequired(req);


        // If we're not preventing spawns - start creating the pieces
        if (!m_disableSpawningSegments)
        {
            // reduce the trajectory marker length as the player progresses
            m_player.m_maxIndicatorLength = Mathf.Clamp(22f - (curLevel * 2f), 5f, 20f);

            // -------------
            // todo: create a scriptable object to setup these intro levels: ball spawn count, player marker length, piece counts
            // -------------

            // How many pieces we should pre-spawn when creating a level
            int startingSpawnCount = 5;

            // Spawn the walls pieces
            for (int i = 0; i < startingSpawnCount; i++)
            {
                GameObject g = null;

                // if we're still in the 'Tutorial Zone'
                if (curLevel <= 5)
                {
                    if (i == 0) {
                        // spawn a set of pockets
                        g = Instantiate(m_wallPrefabs_Tutorials[curLevel - 1], Vector3.zero, Quaternion.identity);
                        g.transform.parent = m_wallHolder;
                    }
                    else {
                        var prevPos = _activeWalls[_activeWalls.Count - 1].ExitZPos();
                        var prefab = m_wallPrefabs[NextRandom(RandomType.SegmentType, 0, m_wallPrefabs.Length)];

                        while (curLevel < prefab.GetComponent<TableSegment>().m_minLevelRequirement) {
                            prefab = m_wallPrefabs[NextRandom(RandomType.SegmentType, 0, m_wallPrefabs.Length)];
                        }

                        g = Instantiate(prefab, new Vector3(0, 0, prevPos), Quaternion.identity);
                    }
                }
                else
                {
                    // spawn an intro piece
                    if (i == 0) {
                        g = Instantiate(m_wallPrefabs_Intros[NextRandom(RandomType.SegmentType, 0, m_wallPrefabs_Intros.Length)], Vector3.zero, Quaternion.identity);
                    }
                    else {
                        var prevPos = _activeWalls[_activeWalls.Count - 1].ExitZPos();
                        var prefab = m_wallPrefabs[NextRandom(RandomType.SegmentType, 0, m_wallPrefabs.Length)];

                        while (curLevel < prefab.GetComponent<TableSegment>().m_minLevelRequirement) {
                            prefab = m_wallPrefabs[NextRandom(RandomType.SegmentType, 0, m_wallPrefabs.Length)];
                        }

                        g = Instantiate(prefab, new Vector3(0, 0, prevPos), Quaternion.identity);
                    }
                }

                g.transform.parent = m_wallHolder;
                _activeWalls.Add(g.GetComponent<TableSegment>());
            }        

            // Spawn and end cap
            if (m_useEndCap) {
                m_wallEndCap.transform.position = new Vector3(0f, 0.5f, _activeWalls[_activeWalls.Count - 1].ExitZPos() + 0f);
                if (!m_wallEndCap.activeSelf)
                    m_wallEndCap.SetActive(true);

                // Clamp camera position to end of run
                //m_zClampMax = m_wallEndCap.transform.position.z;
                m_zClampMax = _activeWalls[_activeWalls.Count - 1].ExitZPos() - 2.5f;
            }
            else {
                if (m_wallEndCap.activeSelf)
                    m_wallEndCap.SetActive(false);
            }          
        }
        else
        {
            // Align all manually placed segments properly
            TableSegment[] ts = m_wallHolder.GetComponentsInChildren<TableSegment>();

            for (int i = 0; i < ts.Length; i++) {
                if (i == 0) {
                    ts[i].transform.position = new Vector3(0f, 0f, 0f);
                }
                else {
                    ts[i].transform.position = new Vector3(0f, 0f, ts[i - 1].ExitZPos());
                }
            }
        }

        // Move the camera into position
        m_camera.transform.position = new Vector3(m_camera.transform.position.x, m_camera.transform.position.y, CameraZPosition(m_player.transform.position.z + cameraFollowOffset));

        // Initialise the walls (create ball spawns)
        for (int i = 0; i < _activeWalls.Count; ++i) {
            _activeWalls[i].Initialize();
        }
            
        ResetLives();
        OnLevelSetup();
    }

    public override void ExitedSection()
    {
        base.ExitedSection();

        // todo: change this to use pooling and if we can't go back far, clear out old sections

        // create a new wall
        SpawnSection();
    }

    public override void BallPocketed(OMP_Ball b, OMP_Ball.BallType type, int ball)
    {
        base.BallPocketed(b, type, ball);

        PoolManager.Inst.ReturnBall(b);
        _activeBalls.Remove(b);

        if (type == OMP_Ball.BallType.Eight || ball == 8) {
            OnPocketedEightball();
        }
        else if (type == OMP_Ball.BallType.Standard)
        {
            AddScore(1);
            ResetLives();
            m_player.BallPocketedThisTurn(true);

            // Check if we have pocketed enough balls to complete the level
            if (BallsPocketedThisAttempt() >= TotalBallsRequiredToCompleteLevel())
                EndRun(EndReason.LevelComplete);

            //if (PoolManager.Inst.ActiveBallCount() == 1)
            //    LevelCompleted();
        }
        else if (type == OMP_Ball.BallType.Powerup)
        {
            ResetLives();
            m_player.BallPocketedThisTurn(true);

            // check powerup type
            if (ball == 0)
            {
                // collision trajectory marker
                m_player.CollisionTrajectoryPowerup(5.0f);
            }
            else if (ball == 1)
            {
                // longer player trajectory marker
            }
        }

        // could do checks later to make sure we are pocketing the right ball, etc
        // bigs vs smalls
        // ordered?
    }
}