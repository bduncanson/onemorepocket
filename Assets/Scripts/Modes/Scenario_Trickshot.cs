﻿/*
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scenario_Trickshot : Scenario {

    [Header("PocketAll Settings")]
    public float m_xRangeMin;
    public float m_xrangeMax;
    public float m_zRangeMin;
    public float m_zRangeMax;

    public override void Update()
    {
        base.Update();
    }

    public override void SetupRun()
    {
        base.SetupRun();

        m_uiController.ShowHideFailPanel(false);

        m_player.OnReset();

        SetAcceptInput(true);
        SetRunReadyToBeSetup(false);

        SetCurrentScore(0);
        SetCurrentDistance(0f);

        // Reset state/position of all the active balls
        for (int i = 0; i < _activeBalls.Count; i++)
        {
            // reset position
            _activeBalls[i].gameObject.SetActive(true);
            _activeBalls[i].GetComponent<OMP_Ball>().Reset();
        }
 
        // place camera
        m_camera.transform.position = new Vector3(m_camera.transform.position.x, m_camera.transform.position.y, m_player.transform.position.z + cameraFollowOffset);

        ResetLives();
    }

    public override void BallPocketed(OMP_Ball b, OMP_Ball.BallType type, int ball)
    {
        base.BallPocketed(b, type, ball);

        if (type == OMP_Ball.BallType.Eight || ball == 8) {         
            EndRun("Pocketed 8-ball.");
        }
        else if (type == OMP_Ball.BallType.Standard) {
            EndRun("Won");
        }

        b.gameObject.SetActive(false);
    }

    public override void OnBallHit(int hit)
    {
        base.OnBallHit(hit);

        if (_firstHitThisAttempt == -1)
            _firstHitThisAttempt = hit;

        if (_firstHitThisAttempt == 8) {
            EndRun("Hit 8-ball.");
        }
    }

    private int _firstHitThisAttempt = -1;

    public override void OnShotEnded(bool hit, bool pocketed)
    {
        base.OnShotEnded(hit, pocketed);

        if (!pocketed || !hit)
        {
            EndRun("Missed trick shot");
        }

        _firstHitThisAttempt = -1;
    }
}

*/