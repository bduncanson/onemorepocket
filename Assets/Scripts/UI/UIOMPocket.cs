﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIOMPocket : MonoBehaviour {

    public OMP_Player m_player;

    public Text m_scoreText;
    private int score = -1;

    public Text m_topScoreText;
    private int topScore = -1;

    [Header("Lives")]
    public GameObject[] m_lives;

    public Text m_multiplierText;
    private int multiplier = -1;

    public GameObject m_failPanel;
    public Text m_failReason;

    public GameObject m_trajectoryShotsPanel;
    public Text m_trajectoryShotsText;

    [Header("Level")]
    public Text m_currentLevelText;
    public Text m_nextLevelText;
    public Text m_levelProgressText;
    public Image m_levelProgressFill;
    private int currentLevel = -1;

    [Header("Virtual Joystick")]
    public RectTransform m_initTouchMarker;
    public RectTransform m_currentTouchMarker;
    public RectTransform m_touchScaler;
    private bool _initiatedDrag = false;

	void Update () {
        if (score != Scenario.Inst.CurrentScore()) {
            score = Scenario.Inst.CurrentScore();
            m_scoreText.text = score.ToString("N0");
        }

        if (topScore != Scenario.Inst.TopScore()) {
            topScore = Scenario.Inst.TopScore();
            m_topScoreText.text = topScore.ToString("N0");
        }

        if (currentLevel != Scenario.Inst.CurrentLevel())
        {
            currentLevel = Scenario.Inst.CurrentLevel();
            m_currentLevelText.text = currentLevel.ToString();
            m_nextLevelText.text = (currentLevel + 1).ToString();
        }

        m_levelProgressFill.fillAmount = Scenario.Inst.CurrentLevelProgress();
        m_levelProgressText.text = Scenario.Inst.CurrentLevelProgressString();

        int lives = Scenario.Inst.LivesRemaining();

        for (int i = 0; i < m_lives.Length; i++)
        {
            if (lives > i)
                m_lives[i].SetActive(true);
            else
                m_lives[i].SetActive(false);
        }

        if (multiplier != Scenario.Inst.CurrentMultiplier()) {
            multiplier = Scenario.Inst.CurrentMultiplier();
            m_multiplierText.text = "Streak: x" + multiplier;
        }

        if (m_player.TrajectoryShotsRemain() > 0) {
            if (!m_trajectoryShotsPanel.activeSelf) m_trajectoryShotsPanel.SetActive(true);
            m_trajectoryShotsText.text = m_player.TrajectoryShotsRemain().ToString();
        }
        else {
            if (m_trajectoryShotsPanel.activeSelf) m_trajectoryShotsPanel.SetActive(false);
        }
    
        if (Input.GetMouseButtonDown(0)) {
            m_initTouchMarker.position = Input.mousePosition;

            if (Scenario.Inst.AcceptInputDrag())
            {
                _initiatedDrag = true;
                m_initTouchMarker.gameObject.SetActive(true);
                m_currentTouchMarker.gameObject.SetActive(true);
            }
        }

        if (Scenario.Inst.AcceptInputDrag() && _initiatedDrag)
        {
            if (Input.GetMouseButton(0)) {
                if (!m_initTouchMarker.gameObject.activeSelf) m_initTouchMarker.gameObject.SetActive(true);
                if (!m_currentTouchMarker.gameObject.activeSelf) m_currentTouchMarker.gameObject.SetActive(true);

                m_currentTouchMarker.position = Input.mousePosition;

                float scaleMulti = 3.0f;
                m_touchScaler.localScale = new Vector2(0.25f + Scenario.Inst.m_player.ShotForcePercentage() * scaleMulti, 0.25f + Scenario.Inst.m_player.ShotForcePercentage() * scaleMulti);
            }

            if (Input.GetMouseButtonUp(0)) {
                m_initTouchMarker.gameObject.SetActive(false);
                m_currentTouchMarker.gameObject.SetActive(false);
                _initiatedDrag = false;
            }
        }
        else {
            if (m_initTouchMarker.gameObject.activeSelf) m_initTouchMarker.gameObject.SetActive(false);
            if (m_currentTouchMarker.gameObject.activeSelf) m_currentTouchMarker.gameObject.SetActive(false);
        }

        if (Input.GetMouseButtonUp(0)) {
            _initiatedDrag = false;
        }
    }

    public void LevelCompleted(string msg) {
        m_failReason.text = msg;
        ShowHideFailPanel(true);
    }

    public void RunFailed(string reason) {
        m_failReason.text = reason;
        ShowHideFailPanel(true);
    }

    public void ShowHideFailPanel(bool show) {
        m_failPanel.SetActive(show);
    }

    public void ResetProgress() {
        Scenario.Inst.ResetLevelProgress();
    }
}
