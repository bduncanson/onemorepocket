﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class BumperEditor : MonoBehaviour {

    public bool m_smartScaling = true;
    public Transform m_baseCorner;
    public Transform m_topCorner;
    public Transform m_midPiece;

    #if UNITY_EDITOR
    void Update () {
        if (m_smartScaling && m_midPiece && m_topCorner && m_midPiece) {
            m_midPiece.localPosition = new Vector3(m_topCorner.localPosition.x, m_topCorner.localPosition.y, m_topCorner.localPosition.z);
            m_midPiece.localScale = new Vector3(1f, 1f, m_topCorner.localPosition.z - m_baseCorner.localPosition.z);
            m_baseCorner.localPosition = new Vector3(m_topCorner.localPosition.x, m_topCorner.localPosition.y, m_baseCorner.localPosition.z);
        }
	}
    #endif
}
