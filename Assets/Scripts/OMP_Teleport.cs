﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OMP_Teleport : MonoBehaviour {

    public Transform m_pair;

    public Transform AlternatePair() {
        return m_pair;
    }

    public Vector3 ExitPosition(Vector3 entrancePos)
    {
        // if teleporting to the right side
        if (m_pair.position.x > entrancePos.x) {
            return new Vector3(m_pair.position.x - 1.1f, entrancePos.y, entrancePos.z);
        }
        else
            return new Vector3(m_pair.position.x + 1.1f, entrancePos.y, entrancePos.z);
    }
}
