﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackHole : MonoBehaviour {

    public float m_holeForce = 10.0f;
    public float m_radialForce = 2.0f;

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player")) {

            Vector3 radialForce = (transform.position - other.transform.position) * m_holeForce;
            radialForce += Vector3.Cross(radialForce.normalized, Vector3.up) * m_radialForce;
            other.GetComponent<OMP_Player>().m_rigidBody.AddForce(radialForce, ForceMode.Acceleration);
        }
        else if (other.CompareTag("OMPBall")) {
            other.GetComponent<OMP_Ball>().m_rigidbody.AddForce((transform.position - other.transform.position) * m_holeForce, ForceMode.Acceleration);
        }
    }
}
