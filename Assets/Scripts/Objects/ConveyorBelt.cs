﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConveyorBelt : MonoBehaviour {

    public float m_speed = 10.0f;
    public float m_beltY = 0.0f;

    private void OnTriggerStay(Collider other)
    {
        if (other.attachedRigidbody == null)
            return;

        Rigidbody rb = other.attachedRigidbody;

        Vector3 force = transform.forward * m_speed;
     
        // add force at the height of the belt, so the balls roll realistically
        Vector3 forcePos = transform.InverseTransformPoint(other.transform.position);
        forcePos.y = m_beltY;
        forcePos = transform.TransformPoint(forcePos);

        rb.AddForceAtPosition(force, forcePos, ForceMode.Acceleration);
    }
}
