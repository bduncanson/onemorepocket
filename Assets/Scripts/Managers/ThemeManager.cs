﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ThemeManager : MonoBehaviour {

    public static ThemeManager Inst;

    [Header("References")]
    public Renderer m_wallRenderer;
    public Renderer m_tableRenderer;

    [Header("Colours")]
    public Theme[] m_themes;

    System.Random themeRandom;

    private int activeTheme = 0;

    private void Awake() {
        Inst = this;
    }

    public void NextTheme(int level) {
        themeRandom = new System.Random(Scenario.Inst.CurrentLevel());
        int t = themeRandom.Next(0, m_themes.Length);
        SetThemeActive(t);
    }

    private void SetThemeActive(int theme) {
        activeTheme = theme;
        if (m_wallRenderer) m_wallRenderer.sharedMaterial.color = m_themes[theme].m_wall;
        if (m_tableRenderer) m_tableRenderer.sharedMaterial.color = m_themes[theme].m_table;
    }

    private void OnDisable() {
        SetThemeActive(0);
    }
}

[System.Serializable]
public class Theme {
    public Color m_wall;
    public Color m_table;
}