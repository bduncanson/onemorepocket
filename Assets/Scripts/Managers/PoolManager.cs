﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoBehaviour {

    public static PoolManager Inst;

    [Header("References")]
    public Transform m_ballHolder;
    public Transform m_wallHolder;

    [Header("Balls")]
    public GameObject[] m_ballPrefabs;
    public int m_initBallSpawnCount = 10;
    private Queue<OMP_Ball> _availableBalls = new Queue<OMP_Ball>();
    private List<OMP_Ball> _activeBalls = new List<OMP_Ball>();

    [Header("Walls")]
    public GameObject[] m_wallPrefabs;
    public int m_initWallSpawnCount = 10;
    private List<GameObject> _availableWalls = new List<GameObject>();
    private List<GameObject> _activeWalls = new List<GameObject>();

    public GameObject[] m_wallPrefabs_Intros;
    private List<GameObject> _availableIntroWalls = new List<GameObject>();
    private List<GameObject> _activeIntroWalls = new List<GameObject>();

    private void Awake() {
        Inst = this;
    }

    private void Start() {
        PopulatePools();
    }

    private void PopulatePools()
    {
        int b = 0;

        // Create pool of balls
        for (int i = 0; i < m_initBallSpawnCount; i++)
        {
            GameObject ball = Instantiate(m_ballPrefabs[b], m_ballHolder);
            ball.SetActive(false);
            _availableBalls.Enqueue(ball.GetComponent<OMP_Ball>());

            b = (b + 1) % m_ballPrefabs.Length;
        }
    }

    public OMP_Ball GetBall()
    {
        OMP_Ball b;

        // get random
        if (_availableBalls.Count > 0)
        {
            b = _availableBalls.Dequeue();
        }
        else
        {
            b = Instantiate(m_ballPrefabs[0], m_ballHolder).GetComponent<OMP_Ball>();
        }

        _activeBalls.Add(b);
        Scenario.Inst.AddActiveBall(b);
        b.gameObject.SetActive(true);
        return b;
    }

    public void ReturnBall(OMP_Ball ball)
    {
        _activeBalls.Remove(ball);

        Rigidbody rb = ball.GetComponent<Rigidbody>();

        rb.isKinematic = true;
        rb.isKinematic = false;

        _availableBalls.Enqueue(ball);

        ball.transform.parent = m_ballHolder;
        ball.transform.localPosition = Vector3.zero;
        ball.gameObject.SetActive(false);
    }

    public int ActiveBallCount
    {
        get { return _activeBalls.Count; }
    }
}