﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrajectoryTracker : MonoBehaviour {

    public List<LineRenderer> m_lines = new List<LineRenderer>();

    public void OnFingerUp()
    {
        for (int i = 0; i < m_lines.Count; i++) {
            m_lines[i].enabled = false;
        }
    }

    public void ShowTrajectory(Vector3 startPoint, Vector3 direction, int steps = 1)
    {
        while (m_lines.Count < steps) {
            LineRenderer lr = new GameObject().AddComponent<LineRenderer>();
            lr.transform.parent = transform;
            m_lines.Add(lr);
        }

        Vector3 lastHitNormal = Vector3.zero;
        Vector3 lastHitPoint = Vector3.zero;    // point from last collision
        RaycastHit lastHit = new RaycastHit() ;                     // last hit

        for (int i = 0; i < steps; i++)
        {
            RaycastHit hit;

            if (i == 0)
            {
                if (Physics.SphereCast(startPoint + (0.5f * direction.normalized), 0.5f, direction.normalized, out hit, 100.0f))
                {
                    if (hit.transform.CompareTag("OMPBall"))
                    {
                        m_lines[i].SetPosition(0, hit.point);
                        //m_lines[i].SetPosition(1, hit.point - hit.normal * 10.0f);
                        m_lines[i].SetPosition(1, hit.point + ((hit.transform.position - hit.point) * 10.0f));
                        m_lines[i].enabled = true;
                        lastHit = hit;
                        lastHitPoint = hit.point;
                        //lastHitNormal = hit.normal;
                        lastHitNormal = hit.transform.position - hit.point;
                    }
                    else if (hit.transform.CompareTag("BounceWall"))
                    {
                        m_lines[i].SetPosition(0, hit.point);
                        m_lines[i].SetPosition(1, Vector3.Reflect(direction, hit.normal) * 10.0f);

                        m_lines[i].enabled = true;
                        lastHit = hit;
                        lastHitPoint = hit.point;
                        lastHitNormal = hit.normal;
                    }
                    else
                        m_lines[i].enabled = false;
                }
                else
                    m_lines[i].enabled = false;
            }
            else
            {
                if (Physics.SphereCast(lastHitPoint + (0.5f * lastHitNormal.normalized), 0.5f, lastHitNormal.normalized, out hit, 100.0f))
                {
                    if (hit.transform.CompareTag("OMPBall"))
                    {
                        m_lines[i].SetPosition(0, hit.point);
                        //m_lines[i].SetPosition(1, hit.point - hit.normal * 10.0f);
                        m_lines[i].SetPosition(1, hit.point + ((hit.transform.position - hit.point) * 10.0f));

                        m_lines[i].enabled = true;

                        lastHit = hit;
                        lastHitPoint = hit.point;
                        //lastHitNormal = hit.normal;
                        lastHitNormal = hit.transform.position - hit.point;
                    }
                    else if (hit.transform.CompareTag("BounceWall"))
                    {
                        //continue;
                        m_lines[i].SetPosition(0, hit.point);
                        m_lines[i].SetPosition(1, Vector3.Reflect(-lastHitNormal.normalized, hit.normal) * 10.0f);

                        m_lines[i].enabled = true;
                        lastHit = hit;
                        lastHitPoint = hit.point;
                        lastHitNormal = hit.normal;
                    }
                else
                    m_lines[i].enabled = false;
                }
                else
                    m_lines[i].enabled = false;
            }
        }      
    }
}