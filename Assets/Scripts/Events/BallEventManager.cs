﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallEventManager : MonoBehaviour {

    public static BallEventManager Inst;

    private List<BallEvent> _shotEvents = new List<BallEvent>();


    private void Awake() {
        Inst = this;
    }

    public void AddEvent(BallEvent newEvent)
    {
        _shotEvents.Add(newEvent);
    }
	
    public void OnShotStarted()
    {

    }

    public void OnShotCompleted()
    {
        // parse events

    }

    public void OnShotSetup()
    {
        // clear event queue
        _shotEvents.Clear();
    }


}

public class BallEvent
{
    public enum Event { None, Bumper, BallHit, Pocket }
    private Event _type = Event.None;
    private Transform _releventTarget; 

    public BallEvent(Event type, Transform t) {
        _type = type;
        _releventTarget = t;
    }

    public Event EventType() {
        return _type;
    }
}