﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateObject : MonoBehaviour {

    public enum Axis { X, Y, Z }
    public Axis m_rotationAxis = Axis.X;
    public float m_rotationSpeed = 0.0f;

    void Update()
    {
        if (m_rotationSpeed != 0.0f)
        {
            if (m_rotationAxis == Axis.X)
                transform.Rotate(Vector3.right * Time.deltaTime * m_rotationSpeed);
            else if (m_rotationAxis == Axis.Y) 
                transform.Rotate(Vector3.up * Time.deltaTime * m_rotationSpeed);
            else if (m_rotationAxis == Axis.Z)
                transform.Rotate(Vector3.forward * Time.deltaTime * m_rotationSpeed);
        }
    }
}
