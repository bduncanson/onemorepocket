﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TableSegment : MonoBehaviour {

    public Transform m_exitSnapPoint;
    public Transform m_ballSpawnerHolder;

    public bool m_spawnBalls = true;
    public int m_minLevelRequirement = 0;   // this piece wont spawn in levels less than this value

    private Collider[] _ballSpawners = new Collider[0];
    private bool playerInside = false;

    private List<SphereCollider> magneticPockets = new List<SphereCollider>();

    private void Awake() {
        if (m_ballSpawnerHolder)
            _ballSpawners = m_ballSpawnerHolder.GetComponentsInChildren<Collider>();

        Transform[] childList = GetComponentsInChildren<Transform>();

        for (int i = 0; i < childList.Length; i++) {
            if (childList[i].CompareTag("MagneticZone"))
                magneticPockets.Add(childList[i].GetComponent<SphereCollider>());
        }
    }

    #if UNITY_EDITOR
    private void OnEnable() {
        if (Scenario.Inst.m_disableSpawningSegments)
            Invoke("Initialize", 0.2f);
    }
    #endif

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player"))
            playerInside = true;
    }

    private void OnTriggerExit(Collider other) {
        if (other.CompareTag("Player"))
            playerInside = false;
    }

    public void Reset() {
        playerInside = false;
        UpdateMagneticZoneSize();
    }

    public bool PlayerInside() {
        return playerInside;
    }

    public float ExitZPos() {
        return m_exitSnapPoint.position.z;
    }

    private void UpdateMagneticZoneSize() {
        if (Scenario.Inst) {
            for (int i = 0; i < magneticPockets.Count; i++) {
                magneticPockets[i].radius = Scenario.Inst.MagneticPocketRange();
            }
        }
    }

    public void Initialize()
    {
        // If we are using custom ball spawn zones, spawn some balls
        if (_ballSpawners.Length > 0 && m_spawnBalls)
        {
            m_ballSpawnerHolder.gameObject.SetActive(true);
            CalculateBallSpawns();
        }

        UpdateMagneticZoneSize();
    }

    private void CalculateBallSpawns()
    {
        // spawn a ball in each spawnzone
        for (int i = 0; i < _ballSpawners.Length; i++)
        {
            GameObject g = PoolManager.Inst.GetBall().gameObject;
            Vector3 offset = RandomSpawnOffset(_ballSpawners[i]);
            g.transform.position = offset;
        }

        //m_ballSpawnerHolder.gameObject.SetActive(false);
    }

    private Vector3 RandomSpawnOffset(Collider c)
    {
        Vector3 spawnOffset = Vector3.zero;
        spawnOffset.x = (float)Scenario.Inst.GetRandomBallSpawn(c.bounds.center.x - c.bounds.extents.x, c.bounds.center.x + c.bounds.extents.x);
        spawnOffset.z = (float)Scenario.Inst.GetRandomBallSpawn(c.bounds.center.z - c.bounds.extents.z, c.bounds.center.z + c.bounds.extents.z);
        spawnOffset.y = 0.5f;
        return spawnOffset;
    }

    #if UNITY_EDITOR
    void OnDrawGizmos()
    {
        if (!Application.isPlaying)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(new Vector3(m_exitSnapPoint.transform.position.x - 20f, m_exitSnapPoint.transform.position.y, m_exitSnapPoint.transform.position.z), new Vector3(m_exitSnapPoint.transform.position.x + 20f, m_exitSnapPoint.transform.position.y, m_exitSnapPoint.transform.position.z));
            Gizmos.color = new Color(0, 0, 1, 0.5f);

            BoxCollider[] spawners = m_ballSpawnerHolder.GetComponentsInChildren<BoxCollider>();

            for (int i = 0; i < spawners.Length; i++) {
                if (spawners[i].transform.localEulerAngles != Vector3.zero)
                {
                    //Gizmos.DrawLine(new Vector3(spawners[i].transform.position.x - spawners[i].size.x / 2f, 0f, spawners[i].transform.position.z + spawners[i].size.z / 2f), new Vector3(spawners[i].transform.position.x + spawners[i].size.x / 2f, 0f, spawners[i].transform.position.z + spawners[i].size.z / 2f));
                    //Gizmos.DrawLine(new Vector3(0f, 0f, 0f), new Vector3(0f, 0f, 0f));
                    //Gizmos.DrawLine(new Vector3(spawners[i].transform.position.x - spawners[i].size.x / 2f, 0f, spawners[i].transform.position.z - spawners[i].size.z / 2f), new Vector3(spawners[i].transform.position.x + spawners[i].size.x / 2f, 0f, spawners[i].transform.position.z - spawners[i].size.z / 2f));
                    //Gizmos.DrawLine(new Vector3(0f, 0f, 0f), new Vector3(0f, 0f, 0f));

                    Gizmos.DrawSphere(spawners[i].transform.position, 1.5f);
                }
                else
                    Gizmos.DrawCube(spawners[i].transform.position, spawners[i].size);
            }

            //Gizmos.matrix = spawners[i].transform.localToWorldMatrix;

        }
    }
    #endif
}