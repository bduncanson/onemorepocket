﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.NiceVibrations;

public class OMP_Player : MonoBehaviour
{
    public static OMP_Player Inst;

    [Header("References")]
    public Camera m_camera;
    public Rigidbody m_rigidBody;
    public Renderer m_renderer;
    public Collider m_collider;
    public TrajectoryTracker m_trajectoryTracker;
    public LineRenderer m_shotForceIndicator;
    public LineRenderer m_intuitionIndicator;
    public GameObject m_readyMarker;

    [Header("Settings")]
    public float m_maxShotLength = 10.0f;
    public float m_maxIndicatorLength = 20.0f;
    public float m_forceMultiplier = 5.0f;
    public float m_minShotForce = 2.0f; // minimum force required to hit ball, less than this will cancel shot
    public AnimationCurve m_forceCurve;

    private Vector3 _fingerDownPosition;
    private Vector3 _initPlayerPosition;
    private Vector3 _currentShotForce = Vector3.zero;
    private Vector3 aimDirection = Vector3.zero;
    private Vector3 positionAtTurnStart;

    private bool _initiatedDrag = false;

    public bool m_forceQuickStop = false;   // force the player to quickly come to a stop if it is rolling slowly
    public float m_quickStopMinVal = 0.3f;  // if player velocity is lower than this value, force the player to come to a stop quicker than drag

    [Header("Debug")]
    public bool m_forceIntuition = false;
    private int _intuitionShotsRemaining = 0;
    public bool m_respawnOnSelfPocket = false;

    public enum State { Disabled, Inactive, Idle, ShotActive, Pocketed, Placing, Dropping, Aiming };
    private State _currentState = State.Disabled;
    public State initState = State.Placing;

    private float shotStartTime = 0.0f;
    private bool ballPocketedThisTurn = false;  // was a ball pocketed on this turn
    private bool ballPocketedLastTurn = false;  // was a ball pocketed on the last turn - chaining shots
    private bool hitBallThisTurn = false;   // has the player ball hit another ball on this shot?


    private void Awake() {
        Inst = this;
        _initPlayerPosition = transform.position;
    }

    private void Start() {
        SetState(initState);
    }

    private void Update()
    {
        // Give the player a small wind-up time, after if the ball has slowed down enough to be stationary, mark the shot as completed
        if (_currentState == State.ShotActive && Time.realtimeSinceStartup > shotStartTime + 0.5f) {
            if (m_rigidBody.velocity.magnitude < 0.05f) {
                ShotEnded();
            }

            // Force the player to come to a stop quicker
            if (m_forceQuickStop) {
                if (m_rigidBody.velocity.magnitude < m_quickStopMinVal) {
                    m_rigidBody.velocity = m_rigidBody.velocity * 0.5f;
                }
            }
        }

        // Player is able to place the ball somewhere on screen
        if (_currentState == State.Placing)
        {
            Vector3 fingerPoint = m_camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0f));
            fingerPoint.y = 1.7f;   // how high should be 'holding' the ball

            fingerPoint.x = Mathf.Clamp(fingerPoint.x, -5.5f, 5.5f);
            fingerPoint.z = Mathf.Clamp(fingerPoint.z, 1.5f, m_camera.transform.position.z);

            transform.position = Vector3.Lerp(transform.position, fingerPoint, Time.deltaTime * 10.0f);
            //transform.position = fingerPoint;

            if (Input.GetMouseButtonUp(0))
                SetState(State.Dropping);
        }
        // After placing, wait for ball to settle before being able to aim
        else if (_currentState == State.Dropping) {
            if (m_rigidBody.velocity.magnitude < 0.05f) {
                SetState(State.Idle);
            }
        }

        #if UNITY_ANDROID || UNITY_IOS
        if (Input.touchCount > 1)
            return;
        #endif

        if (Input.GetMouseButtonDown(0) && CanDrag())
        {
            if (_currentState == State.Idle)
            {
                _fingerDownPosition = m_camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0f));
                m_shotForceIndicator.enabled = true;

           
                _initiatedDrag = true;
                m_intuitionIndicator.enabled = false;
                SetState(State.Aiming);
            }
        }

        if (Input.GetMouseButton(0) && CanDrag() && _initiatedDrag)
        {
            // calculate drag force           
            Vector3 fingerPoint = m_camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0f));
            Vector3 dragForce = _fingerDownPosition - fingerPoint;

            // calc the aim direction
            aimDirection = (_fingerDownPosition - fingerPoint).normalized;

            // Show shot force indicator on cue
            Vector3 shotForce = transform.position + Vector3.ClampMagnitude(dragForce, m_maxIndicatorLength);
            m_shotForceIndicator.SetPosition(0, transform.position);
            m_shotForceIndicator.SetPosition(1, shotForce);

            Color c = Color.Lerp(Color.green, Color.red, ShotForcePercentage());
            m_shotForceIndicator.startColor = c;
            c.a = 0.5f;
            m_shotForceIndicator.endColor = c;

            // clamp the size of the force
            _currentShotForce = Vector3.ClampMagnitude(dragForce, m_maxShotLength);

            // allow player to reduce the force of a shot enough to cancel the shot entirely.
            if (_currentShotForce.magnitude < m_minShotForce) {
                if (m_shotForceIndicator.enabled) m_shotForceIndicator.enabled = false;
            }
            else {
                if (!m_shotForceIndicator.enabled) m_shotForceIndicator.enabled = true;
            }

            // Intuition?
            if (m_forceIntuition || _intuitionShotsRemaining > 0)
            {
                IntuitionMarker(_currentShotForce);
            }
        }

        if (Input.GetMouseButtonUp(0) && CanDrag())
        {
            if (_initiatedDrag)
            {
                if (!Scenario.Inst.RunActive())
                    Scenario.Inst.StartRun();

                //m_shotForceIndicator.enabled = false;

                if (_currentShotForce.magnitude > m_minShotForce) {
                    ShotStarted();
                    m_rigidBody.AddForce(_currentShotForce.normalized * m_forceCurve.Evaluate(_currentShotForce.magnitude / m_maxShotLength) * m_forceMultiplier, ForceMode.Impulse);
                }
                else
                    SetState(State.Idle);
            }

            m_intuitionIndicator.enabled = false;

            _initiatedDrag = false;
        }

        #if UNITY_EDITOR || UNITY_STANDALONE 
        DebugFunctions();
        #endif
    }

    private void DebugFunctions()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (CurrentState() == State.Idle)
                SetState(State.Placing);
        }
    }

    public void SetState(State newState)
    {
        #if UNITY_EDITOR
        //Debug.Log(_currentState + " -> " + newState);
        #endif

        switch (_currentState)
        {
            case State.Disabled:
                m_renderer.enabled = true;
                m_collider.enabled = true;
                break;
            case State.Inactive:
                break;
            case State.Idle:
                if (newState != State.Aiming)
                    SetReadyMarkerActive(false);
                break;
            case State.ShotActive:
                Scenario.Inst.OnShotEnded(hitBallThisTurn, ballPocketedThisTurn);
                break;
            case State.Pocketed:
                break;
            case State.Placing:
                m_collider.enabled = true;
                break;
            case State.Dropping:
                break;
            case State.Aiming:
                if (m_intuitionIndicator.enabled) {
                    m_intuitionIndicator.enabled = false;
                }

                if (newState != State.Idle)
                    SetReadyMarkerActive(false);
                break;
            default:
                break;
        }

        _currentState = newState;

        switch (newState)
        {
            case State.Disabled:
                m_renderer.enabled = false;
                m_collider.enabled = false;
                m_rigidBody.velocity = Vector3.zero;
                break;
            case State.Inactive:
                m_rigidBody.isKinematic = true;
                m_rigidBody.velocity = Vector3.zero;
                _initiatedDrag = false;
                break;
            case State.Idle:
                SetReadyMarkerActive(true);
                m_rigidBody.isKinematic = false;

                m_rigidBody.isKinematic = true;
                m_rigidBody.velocity = Vector3.zero;
                m_rigidBody.angularVelocity = Vector3.zero;
                m_rigidBody.isKinematic = false;

                break;
            case State.ShotActive:
                m_shotForceIndicator.enabled = false;
                break;
            case State.Pocketed:
                m_rigidBody.isKinematic = true;
                break;
            case State.Placing:
                m_rigidBody.isKinematic = true;
                m_collider.enabled = false;
                break;
            case State.Dropping:
                m_collider.enabled = true;
                break;
            case State.Aiming:
                if (m_forceIntuition || _intuitionShotsRemaining > 0) {
                    m_intuitionIndicator.enabled = true;
                }
                break;
            default:
                break;
        }
    }

    void SetReadyMarkerActive(bool active)
    {
        if (m_readyMarker)
        {
            m_readyMarker.transform.position = new Vector3(transform.position.x, m_readyMarker.transform.position.y, transform.position.z);
            m_readyMarker.SetActive(active);
        }
    }

    // We have initiated a shot
    private void ShotStarted()
    {
        Scenario.Inst.StartTurn();

        positionAtTurnStart = transform.position;

        SetState(State.ShotActive);

        if (_intuitionShotsRemaining > 0)
            _intuitionShotsRemaining--;

        hitBallThisTurn = false; // reset hit check
        ballPocketedLastTurn = ballPocketedThisTurn; // keep track of if we pocketed a ball on the last turn
        ballPocketedThisTurn = false; // reset pocket check
        shotStartTime = Time.realtimeSinceStartup;

        if (m_trajectoryTracker)
            m_trajectoryTracker.OnFingerUp();

    }

    // The shot has completed, we have come to a rest
    private void ShotEnded() {

        //if (!hitBallThisTurn) {
        //    Scenario.Inst.RestartTurn();
            
        //}
        //else {
            SetState(State.Idle);
        //}
    }

    // When a run has completed
    public void OnRunEnded() {
        StartCoroutine(RunEndedDelay());
    }

    public void OnTurnReset() {
        SetState(State.Idle);
        transform.position = positionAtTurnStart;
    }

    IEnumerator RunEndedDelay() {
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        SetState(State.Inactive);
    }

    public void OnReset() {
        m_rigidBody.isKinematic = true;
        m_rigidBody.velocity = Vector3.zero;
        transform.position = _initPlayerPosition;
        m_rigidBody.isKinematic = false;
        _initiatedDrag = false;
        SetState(State.Idle);
    }

    // A ball has been pocketed during this shot
    public void BallPocketedThisTurn(bool pocketed)
    {
        ballPocketedThisTurn = pocketed;

        if (pocketed)
        {
            if (ballPocketedLastTurn)
                Scenario.Inst.AddMultiplier();
        }
    }

  



    /// --------------------------------------------------------------------------
    /// COLLISIONS
    /// --------------------------------------------------------------------------

    private void OnCollisionEnter(Collision collision) {
        // If we hit another ball
        if (collision.transform.CompareTag("OMPBall")) {
            hitBallThisTurn = true;
            OMP_Ball b = collision.transform.GetComponent<OMP_Ball>();
            if (b != null) Scenario.Inst.OnBallHit(b.m_ballNumber);

            //#if !UNITY_EDITOR
            //Handheld.Vibrate();
            //#endif

            MMVibrationManager.Haptic(HapticTypes.MediumImpact);
        }
        else if (collision.transform.CompareTag("BounceWall")) {
            MMVibrationManager.Haptic(HapticTypes.MediumImpact);
        }
        else {
            MMVibrationManager.Haptic(HapticTypes.LightImpact);
        }
    }

    private void OnCollisionStay(Collision collision) {
        // in case we're up against a ball at the end of the last run?
        if (collision.transform.CompareTag("OMPBall")) {
            hitBallThisTurn = true;
        }
    }

    private void OnTriggerEnter(Collider other) {
        // If we enter a pocket
        if (other.CompareTag("OMPPocket")) {
            if (!m_respawnOnSelfPocket) {
                Scenario.Inst.OnPocketedPlayer();
                MMVibrationManager.Haptic(HapticTypes.Failure);
            }
            else {
                SetState(State.Inactive);
                m_rigidBody.isKinematic = true;
                m_rigidBody.isKinematic = false;

                MMVibrationManager.Haptic(HapticTypes.Failure);

                if (Scenario.Inst.LivesRemaining() > 0)
                    StartCoroutine(DelayRespawnPlayerOnPocket());
            }
        }
        // If we enter a teleporting object
        else if (other.CompareTag("OMPTeleport")) {
            OMP_Teleport tp = other.transform.GetComponent<OMP_Teleport>();
            transform.position = tp.ExitPosition(transform.position); // new Vector3(tp.AlternatePair().position.x, transform.position.y, transform.position.z);
        }
        // If we exit a table 'section'
        else if (other.CompareTag("OMPSectionExit")) {
            other.enabled = false;
            Scenario.Inst.ExitedSection();
        }
        else if (other.CompareTag("BlackHoleCenter")) {
            //Scenario.Inst.EndRun("Ball Meet Hole");
            blackHoleStayTimer = 0.0f;
        }
        else if (other.CompareTag("Goal")) {
            //Scenario.Inst.LevelCompleted();
        }
    }

    IEnumerator DelayRespawnPlayerOnPocket()
    {
        yield return new WaitForSeconds(0.5f);
        //Scenario.Inst.AddLives(-1);
        //OnTurnReset();
        Scenario.Inst.RestartTurn();
    }

    private float blackHoleStayTimer = 0.0f;
    private float blackHoleMaxStayLength = 0.2f;

    private void OnTriggerStay(Collider other)
    {
        //if (other.CompareTag("BlackHoleCenter"))
        //{
        //    blackHoleStayTimer += Time.fixedDeltaTime;

        //    if (blackHoleStayTimer > blackHoleMaxStayLength)
        //        Scenario.Inst.EndRun("Ball Meet Hole");
        //}

        if (other.CompareTag("MagneticZone") && Scenario.Inst.MagneticPocketsActive()) {
            m_rigidBody.AddForce((other.transform.position - transform.position) * Scenario.Inst.MagneticPocketForceMultiplier(), ForceMode.Acceleration);
        }      
    }

    // Display a line from potential hit target to show how the players collision will affect it
    void IntuitionMarker(Vector3 direction) {
        m_intuitionIndicator.enabled = true;
        m_trajectoryTracker.ShowTrajectory(transform.position, direction, 2);
    }
  

    /// --------------------------------------------------------------------------
    /// POWERUPS
    /// --------------------------------------------------------------------------

    // Enable Trajectory Powerup
    public void CollisionTrajectoryPowerup(float length) {
        _intuitionShotsRemaining = 3;
    }

    private void DisableCollisionTrajectoryPowerup() {
        _intuitionShotsRemaining = 0;
    }


    /// --------------------------------------------------------------------------
    /// PUBLIC ACCESSORS
    /// --------------------------------------------------------------------------

    // Players starting position 
    public Vector3 InitPlayerPosition() {
        return _initPlayerPosition;
    }

    // How many trajectory shots do we have remaining
    public int TrajectoryShotsRemain() {
        return _intuitionShotsRemaining;
    }

    // Is the ball currently in a state ready to be aimed?
    public bool CanDrag() {
        if (Scenario.Inst.AcceptInput() && m_rigidBody.velocity.magnitude < 0.2f && (CurrentState() == State.Idle || CurrentState() == State.Aiming)) {
            return true;
        }
        return false;
    }

    // What % of max force is the current shot going to be
    public float ShotForcePercentage() {
        return _currentShotForce.magnitude / m_maxShotLength;
    }

    // Players current state
    public State CurrentState() {
        return _currentState;
    }

    // Is the player currently in the middle of a shot
    public bool ShotActive() {
        if (_currentState == State.ShotActive)
            return true;
        return false;
    }

    // Current Aim Direction
    public Vector3 AimDirection()
    {
        if (CurrentState() == State.Aiming)
        {

            return aimDirection;
        }

        return Vector3.zero;
    }

    // Just putting this here so android gets vibrate permission
    private void AndroidManifestVibrate() {
        Handheld.Vibrate();
    }
}