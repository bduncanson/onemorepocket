﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.NiceVibrations;

public class OMP_Ball : MonoBehaviour {

    public enum BallType { Standard, Eight, Powerup }
    public BallType m_type = BallType.Standard;

    public int m_ballNumber = -1;

    private Vector3 initPosition;

    public Rigidbody m_rigidbody;
    public Renderer m_renderer;

    private bool hitByPlayer = false; // have we been hit by the player, for checking 
    private bool hitDuringTurn = false; // have we been hit by something during our turn

    public bool m_forceQuickStop = true;   // force the player to quickly come to a stop if it is rolling slowly
    public float m_quickStopMinVal = 0.3f;  // if player velocity is lower than this value, force the player to come to a stop quicker than drag

    public Material[] m_ballColours;


    private Vector3 positionAtStartOfTurn;   // where the ball is when the turn starts

    private void Awake() {
        initPosition = transform.position;

        if (m_type == BallType.Standard && m_ballNumber == -1) {
            m_ballNumber = Random.Range(1, 16);

            while (m_ballNumber == 8) {
                m_ballNumber = Random.Range(1, 16);
            }

            if (m_renderer && m_ballColours.Length > m_ballNumber) {
                m_renderer.material = m_ballColours[m_ballNumber - 1];
            }
        }


        if (m_rigidbody == null)
            m_rigidbody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (m_forceQuickStop && hitByPlayer) {
            if (m_rigidbody.velocity.magnitude != 0) {
                if (m_rigidbody.velocity.magnitude < m_quickStopMinVal) {
                    m_rigidbody.velocity = m_rigidbody.velocity * 0.5f;
                }
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag("Player")) {
            //BallEventManager.Inst.AddEvent(new BallEvent(BallEvent.Event.BallHit, collision.transform));
            SetHitByPlayer(true);
        }
        else if (collision.transform.CompareTag("OMPBall")) {
            SetHitDuringTurn(true);
            MMVibrationManager.Haptic(HapticTypes.LightImpact);
        }
        else if (collision.transform.CompareTag("BounceWall")) {
            //BallEventManager.Inst.AddEvent(new BallEvent(BallEvent.Event.Bumper, collision.transform));
            MMVibrationManager.Haptic(HapticTypes.LightImpact);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("OMPPocket")) {
            Scenario.Inst.BallPocketed(this, m_type, m_ballNumber);
            //Destroy(gameObject);
        }
        else if (other.CompareTag("OMPTeleport")) {
            OMP_Teleport tp = other.transform.GetComponent<OMP_Teleport>();
            transform.position = tp.ExitPosition(transform.position); // new Vector3(tp.AlternatePair().position.x, transform.position.y, transform.position.z);
        }
        else if (other.CompareTag("BlackHoleCenter")) {
            // destroy the ball

            Scenario.Inst.DestroyBall(gameObject);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("MagneticZone") && Scenario.Inst.MagneticPocketsActive()) {
            m_rigidbody.AddForce((other.transform.position - transform.position) * Scenario.Inst.MagneticPocketForceMultiplier(), ForceMode.Acceleration);
        }
    }

    public void Reset() {
        m_rigidbody.velocity = Vector3.zero;
        m_rigidbody.isKinematic = true;
        transform.position = initPosition;
        m_rigidbody.isKinematic = false;
    }

    public bool HitByPlayer() {
        return hitByPlayer;
    }

    public void SetHitByPlayer(bool hit) {
        hitByPlayer = hit;
    }

    private void SetHitDuringTurn(bool hit) {
        hitDuringTurn = hit;
    }

    public void OnTurnStart() {
        SetHitDuringTurn(false);
        SetHitByPlayer(false);
        positionAtStartOfTurn = transform.position;
    }

    // If we're reverting the turn state
    public void OnTurnReset()
    {
        if (HitByPlayer() || hitDuringTurn) { 
            if (m_rigidbody.velocity != Vector3.zero) {
                m_rigidbody.isKinematic = true;
                m_rigidbody.isKinematic = false;
            }

            transform.position = positionAtStartOfTurn;
        }
    }
}