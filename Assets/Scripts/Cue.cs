﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitalRuby.Tween;

public class Cue : MonoBehaviour {

    public OMP_Player m_player;
    public GameObject m_cueObj;
    public Transform m_cueTransform;

    public float m_cueDrawMin = 0.0f;
    public float m_cueDrawMax = -2.0f;

    private bool hasAnimated = false;

	void LateUpdate () {
		if (m_player.CurrentState() == OMP_Player.State.Aiming)
        {
            hasAnimated = false; 

            if (!m_cueObj.activeSelf) m_cueObj.SetActive(true); // show cue

            transform.position = m_player.transform.position;

            Vector3 aimDir = m_player.AimDirection();
            if (aimDir != Vector3.zero)
                transform.rotation = Quaternion.LookRotation(aimDir, Vector3.up);

            m_cueTransform.localPosition = new Vector3(0f, 0f, Mathf.Lerp(m_cueDrawMin, m_cueDrawMax, Mathf.Abs(m_player.ShotForcePercentage())));
        }
        else {
            if (m_player.CurrentState() == OMP_Player.State.ShotActive) {
                if (!hasAnimated) {
                    AnimateStrike();
                }
            }
            else {
                DisableCue();
            }
        }
    }

    void AnimateStrike()
    {
        hasAnimated = true;

        Vector3 startPos = m_cueTransform.localPosition;
        Vector3 endPos = new Vector3(0f, 0f, 3.0f);

        m_cueTransform.gameObject.Tween("StrikeTween", startPos, endPos, 0.15f, TweenScaleFunctions.CubicEaseOut, (t) =>
        {
            // progress
            m_cueTransform.localPosition = t.CurrentValue;

        }, (t) =>
        {
            // complete
            Invoke("DisableCue", 0.5f);
        });
    }

    void DisableCue() {
        m_cueObj.SetActive(false); // hide cue
    }
}
