﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolObject : MonoBehaviour {

    public RotateObject.Axis m_axis;

    public bool m_initDirPositive = true;

    public float m_min;
    public float m_max;
    public float m_speed;

    private bool _posMoveDirection = true;
    private Transform _transform;

    private void Awake() {
        if (_transform == null) _transform = GetComponent<Transform>();
        _posMoveDirection = m_initDirPositive;
    }

    void Update ()
    {
		if (m_axis == RotateObject.Axis.X)
        {

        }
        else if (m_axis == RotateObject.Axis.Y)
        {

        }
        else if (m_axis == RotateObject.Axis.Z)
        {
            if (_posMoveDirection)
            {
                if (_transform.localPosition.z < m_max)
                    transform.localPosition += Vector3.forward * Time.deltaTime * m_speed;
                else
                    _posMoveDirection = false;
            }
            else
            {
                if (_transform.localPosition.z > m_min)
                    transform.localPosition -= Vector3.forward * Time.deltaTime * m_speed;
                else
                    _posMoveDirection = true;
            }
        }
	}
}
